#!/usr/bin/env python3

#  ###########################################################################
 #  Copyright(C) 2020 Michal Charvat
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU General Public License for more details.
 #  
 #  You should have received a copy of the GNU General Public License
 #  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #  ###########################################################################
#

import sys
from v4l2lepton3.control import Lepton3Control

def get_usage():
    return 'Use lepton3control.py <i2c_number> <command> <method> [<data>]\n' \
           '\t<i2c_number> -- for /dev/i2c-1 use 1\n' \
           '\t<command> -- one of the commands below\n' \
           '\t<method> -- one of the get, set, run - see available methods next to commands below\n' \
           '\t<data> -- parameter only for the set method, leave empty to see available options\n' \
           'Commands:\n{}'.format(Lepton3Control.get_commands())


if __name__ == '__main__':
    if len(sys.argv) not in {4, 5}:
        print(get_usage())
        sys.exit(1)

    # prepare command data
    i2c_number, command_name, method_name = int(sys.argv[1]), sys.argv[2], sys.argv[3]
    parameter = sys.argv[4] if len(sys.argv) > 4 else None

    try:
        # execute requested command
        print('Opening i2c device: {}'.format(i2c_number))
        lepton = Lepton3Control(i2c_number)
        print('Booted: {} Ready: {} ErrorCode: {}'.format(*lepton.get_status()))
        result, error_code = lepton.execute_command(command_name, method_name, parameter)

        # print result
        print('Error code: {}'.format(error_code))
        print('Done.' if result is None else 'Result: {}'.format(result))

    except Exception as e:
        print('Error: {}'.format(e))
        exit(2)