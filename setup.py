import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="v4l2lepton3",
    version="1.0",
    author="Michal Charvat",
    author_email="charvin@atlas.cz",
    licence="GNU LESSER GENERAL PUBLIC LICENSE v3",
    description="Python3 implementation of the v4l2lepton3 client. Connects to a remote thermal unit running the v4l2lepton3 server and provides raw thermal images as uint16 np.ndarray.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/CharvN/v4l2lepton3",
    packages=['v4l2lepton3'],
    python_requires='>3.5.2',
)