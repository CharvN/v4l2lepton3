# ***************************************************************************
 # *  Copyright(C) 2020 Michal Charvát
 # *
 # *  This program is free software: you can redistribute it and/or modify
 # *  it under the terms of the GNU General Public License as published by
 # *  the Free Software Foundation, either version 3 of the License, or
 # *  (at your option) any later version.
 # *
 # *  This program is distributed in the hope that it will be useful,
 # *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 # *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # *  GNU General Public License for more details.
 # *
 # *  You should have received a copy of the GNU General Public License
 # *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 # *  ***************************************************************************

import zlib
import socket
import struct
import threading
import numpy as np
from v4l2lepton3.utils import Timeout

class Lepton3Client(object):
    _FRAME_SIZE = 38400

    def __init__(self, ip: str, port: int=2222):
        self._ip = ip
        self._port = port
        self._frame = None
        self._socket = None
        self._should_stop = False
        self._receive_thread = None
        self._frame_requested = False
        self._frame_ready = threading.Condition()
        self._decompressor = zlib.decompressobj(15 + 32)

    def _receive_frames(self) -> None:
        assert self._socket, 'Socket connection not found, you must connect first.'
        message = b''

        while not self._should_stop:
            received = self._socket.recv(self._FRAME_SIZE)
            if not received:
                raise StopIteration

            try:
                message += self._decompressor.decompress(received) if self._decompressor else received

            except zlib.error: # if zlib doesnt work, the server is sending without compression
                self._decompressor = None
                message += received

            if len(message) >= self._FRAME_SIZE:
                raw_frame = message[:self._FRAME_SIZE]
                message = message[self._FRAME_SIZE:]

                frame = struct.unpack('>19200H', raw_frame)
                frame = np.asarray(frame).reshape(120, 160).astype('uint16')

                # if someone wants a frame, give it
                self._frame_ready.acquire()
                if self._frame_requested:
                    self._frame = frame
                    self._frame_ready.notify()
                self._frame_ready.release()

    def get_frame(self, timeout: int = 30, **kwargs) -> np.ndarray:
        with Timeout(timeout):
            self._frame_ready.acquire()

            self._frame_requested = True
            self._frame_ready.wait()
            frame = self._frame
            self._frame_requested = False

            self._frame_ready.release()
            return frame

    def __enter__(self):
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.connect((self._ip, self._port))

        self._should_stop = False
        self._receive_thread = threading.Thread(name='Frames receive thread', target=self._receive_frames)
        self._receive_thread.start()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._receive_thread:
            self._should_stop = True
            self._receive_thread.join()
            self._receive_thread = None

        self._socket.shutdown(2)
        self._socket.close()
        self._socket = None
