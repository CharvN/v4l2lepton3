# ***************************************************************************
 # *  Copyright(C) 2020 Michal Charvát
 # *
 # *  This program is free software: you can redistribute it and/or modify
 # *  it under the terms of the GNU General Public License as published by
 # *  the Free Software Foundation, either version 3 of the License, or
 # *  (at your option) any later version.
 # *
 # *  This program is distributed in the hope that it will be useful,
 # *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 # *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 # *  GNU General Public License for more details.
 # *
 # *  You should have received a copy of the GNU General Public License
 # *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 # *  ***************************************************************************

import spi
import numpy as np
from time import sleep

from v4l2lepton3.utils import Timeout

class Lepton3Capturer(object):
    _PACKET_DATA_SIZE = 160
    _PACKET_SIZE = 164
    _SEGMENT_SIZE = 60
    _FRAME_SIZE = 4

    def __init__(self, spi_device: str = '/dev/spidev0.0'):
        self._device = None
        self._spi_device: str = spi_device

    def get_frame(self, timeout: int = 30, **kwargs) -> np.ndarray:
        assert self._device, 'You first must open the SPI connection.'

        with Timeout(timeout):
            frame = []
            segment = []
            packet_transmit_buffer = (0,) * self._PACKET_SIZE

            segment_index = 1
            while (segment_index <= self._FRAME_SIZE): # index 1-4
                packet = spi.transfer(self._device, packet_transmit_buffer)
                if (packet[0] & 0x0F) == 0xF:
                    sleep(0.000001)
                    segment.clear()
                    continue

                segment.append(packet)
                if (packet[1] == 59):
                    if (segment[20][0] >> 4) == segment_index:
                        frame.append(segment)
                        segment_index += 1
                    segment = []

            reordered_frame = []
            for segment in frame:
                for packet in segment:
                    d = packet[4:]
                    reordered_frame += [((a << 8) | b) for a, b in zip(d[::2], d[1::2])]

            assert len(reordered_frame) == 120 * 160, "Actual length: {}".format(len(reordered_frame))
            frame = np.asarray(reordered_frame, dtype=np.uint16).reshape(120, 160)
            return frame

    def __enter__(self):
        self._device = spi.openSPI(mode=3, device=self._spi_device, speed=20000000)
        assert self._device, 'Failed opening SPI device: {}'.format(self._spi_device)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self._device is not None:
            spi.closeSPI(self._device)
            self._device = None