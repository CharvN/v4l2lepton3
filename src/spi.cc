/* ***************************************************************************
 *  Copyright(C) 2020 Michal Charvát
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  ***************************************************************************
 */

#include "spi.hh"
#include "log.hh"

#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

using namespace std;

void SPI::Close()
{
	if(fd >= 0)
	{
		BOOST_LOG_TRIVIAL(trace) << "Closing SPI.";
		close(fd);
		fd = -1;
	}
}

bool SPI::Open(const std::string &deviceName, uint32_t speed, uint16_t delayUs, uint8_t csChange, uint8_t mode, uint8_t bitsPerWord) {
	this->~SPI();

	if((fd = open(deviceName.c_str(), O_RDWR)) < 0)
	{
		BOOST_LOG_TRIVIAL(trace) << "Failed to open SPI device: " << deviceName;
		return false;
	}

	if(!setMode(mode) || !setSpeed(speed) || !setBitsPerWord(bitsPerWord))
	{
		this->~SPI();
		return false;
	}

	transferParam.delay_usecs = delayUs;
	transferParam.cs_change = csChange;
	transferParam.tx_nbits = 1;
	transferParam.rx_nbits = 1;

	return true;
}

bool SPI::setMode(uint8_t mode) {
	const static char errMessage[] = "Failed to set SPI mode.";

	if(ioctl(fd, SPI_IOC_WR_MODE, &mode) == -1)
	{
		BOOST_LOG_TRIVIAL(trace) << errMessage;
		return false;
	}

	uint8_t currentMode = 0xFF;
	if((ioctl(fd, SPI_IOC_RD_MODE, &currentMode) == -1) || currentMode != mode)
	{
		BOOST_LOG_TRIVIAL(trace) << errMessage;
		return false;
	}

	this->mode = mode;
	return true;
}

bool SPI::setSpeed(uint32_t speedHz) {
	const static char errMessage[] = "Failed to set SPI speed.";

	if(ioctl(fd, SPI_IOC_WR_MAX_SPEED_HZ, &speedHz) == -1)
	{
		BOOST_LOG_TRIVIAL(trace) << errMessage;
		return false;
	}

	uint32_t currentSpeed = 0xFFFFFFFF;
	if((ioctl(fd, SPI_IOC_RD_MAX_SPEED_HZ, &currentSpeed) == -1) || currentSpeed != speedHz)
	{
		BOOST_LOG_TRIVIAL(trace) << errMessage;
		return false;
	}

	transferParam.speed_hz = speedHz;
	return true;
}

bool SPI::setBitsPerWord(uint8_t bitsPerWord) {
	const static char errMessage[] = "Failed to set SPI bits per word.";

	if(ioctl(fd, SPI_IOC_WR_BITS_PER_WORD, &bitsPerWord) == -1)
	{
		BOOST_LOG_TRIVIAL(trace) << errMessage;
		return false;
	}

	uint8_t currentBits = 0xFF;
	if((ioctl(fd, SPI_IOC_RD_BITS_PER_WORD, &currentBits) == -1) || currentBits != bitsPerWord)
	{
		BOOST_LOG_TRIVIAL(trace) << errMessage;
		return false;
	}

	transferParam.bits_per_word = bitsPerWord;
	return true;
}

bool SPI::Transfer(const uint8_t *transmitBuffer, const uint8_t *receiveBuffer, uint32_t length) const
{
	spi_ioc_transfer transfer{transferParam};
	transfer.tx_buf = reinterpret_cast<uintptr_t>(transmitBuffer);
	transfer.rx_buf = reinterpret_cast<uintptr_t>(receiveBuffer);
	transfer.len = length;

	return ioctl(fd, SPI_IOC_MESSAGE(1), &transfer) > 0;
}
