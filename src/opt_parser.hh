/* ***************************************************************************
 *  Copyright(C) 2020 Michal Charvát
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  ***************************************************************************
 */

#pragma once

#include <string>
#include <unordered_map>

class Option
{
public:
	enum ParamType {NoParam, RequiredParam, OptionalParam};

	bool optional;
	std::string name;
	std::string longName;
	ParamType paramType;

	bool entered = false;
	std::string param;

	Option(std::string name, std::string longName, bool optional, ParamType paramType) :
		optional{optional},
		name{std::move(name)},
		longName{std::move(longName)},
		paramType{paramType}
	{
	}

	Option(std::string name, std::string longName, bool optional, ParamType paramType, std::string defaultValue) :
			optional{optional},
			name{std::move(name)},
			longName{std::move(longName)},
			paramType{paramType},
			param{std::move(defaultValue)}
	{
	}
};

class OptParser
{
protected:
	std::unordered_map<std::string, Option> Options;
	bool valid = false;

public:
	void RegisterOption(std::string name, std::string longName, bool optional = true, Option::ParamType paramType = Option::ParamType::NoParam);
	void RegisterOption(std::string name, std::string longName, bool optional, Option::ParamType paramType, std::string defaultValue);
	bool ParseArguments(int argc, const char** argv);
	Option* GetOption(const std::string &key);

	bool Valid() const { return valid; };
	operator bool() const {return Valid();}
};
