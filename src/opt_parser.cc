/* ***************************************************************************
 *  Copyright(C) 2020 Michal Charvát
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  ***************************************************************************
 */

#include "opt_parser.hh"

using namespace std;

void OptParser::RegisterOption(string name, string longName, bool optional, Option::ParamType paramType)
{
	Options.emplace(move(name), Option(name, move(longName), optional, paramType));
}

void OptParser::RegisterOption(string name, string longName, bool optional, Option::ParamType paramType, string defaultValue)
{
	Options.emplace(move(name), Option(name, move(longName), optional, paramType, move(defaultValue)));
}

Option* OptParser::GetOption(const std::string &key)
{
	unordered_map<std::string, Option>::iterator iter;

	if((iter = Options.find(key)) != Options.end())
		return &(iter->second);

	for(iter = Options.begin(); iter != Options.end(); iter++)
	{
		if(key == iter->second.longName)
			return &(iter->second);
	}

	return nullptr;
}

bool OptParser::ParseArguments(int argc, const char **argv)
{
	for(const char** opt = argv + 1; opt < argv + argc; ++opt)
	{
		Option* option;
		if((option = GetOption(string(*opt))) == nullptr)
			return false;

		switch(option->paramType)
		{
			case Option::ParamType::OptionalParam:
			{
				const char **nextOpt = opt + 1;
				if (nextOpt < argv + argc && GetOption(string(*nextOpt)) == nullptr)
				{
					option->param = string(*nextOpt);
					opt++;
				}
				break;
			}

			case Option::ParamType::RequiredParam:
			{
				if(++opt >= argv + argc)
					return false;
				option->param = string(*opt);
				break;
			}

			default:
				break;
		}

		option->entered = true;
	}

	// Check if all required were entered
	for(const auto &o : Options)
	{
		if(!o.second.optional && !o.second.entered)
			return false;
	}

	valid = true;
	return true;
}
