/* ***************************************************************************
 *  Copyright(C) 2020 Michal Charvát
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  ***************************************************************************
 */

#include "log.hh"
#include "v4l2.hh"

#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>

using namespace std;

v4l2::v4l2(uint32_t width, uint32_t height, v4l2::V4L2Format format) : width{width}, height{height}, format{format}
{
	if(format == V4L2Format::RGB)
		rgbFrameBuffer.resize(width * height * 3);
}

v4l2::~v4l2()
{
	Close();
}

void v4l2::Close()
{
    if(fd >= 0)
    {
        close(fd);
        fd = -1;
    }
}

bool v4l2::Open(const char* deviceName)
{
    Close();

    if((fd = open(deviceName, O_WRONLY)) < 0)
		return false;

    if(!Configure())
    {
        Close();
        return false;
    }

    return true;
}

bool v4l2::Configure()
{
    v4l2_format v4l2Format = {0};
	v4l2Format.type = V4L2_BUF_TYPE_VIDEO_OUTPUT;

	// Get currently set format
    if(ioctl(fd, VIDIOC_G_FMT, &v4l2Format) == -1)
    {
	    BOOST_LOG_TRIVIAL(error) << "Failed to retrieve video device format: " << errno;
        return false;
    }

	v4l2Format.fmt.pix.width = width;
	v4l2Format.fmt.pix.height = height;
	switch(format)
	{
		case V4L2Format::RAW:
			v4l2Format.fmt.pix.pixelformat = V4L2_PIX_FMT_Y16;
			v4l2Format.fmt.pix.colorspace = V4L2_COLORSPACE_RAW;
			v4l2Format.fmt.pix.xfer_func = V4L2_XFER_FUNC_NONE;
			break;
		case V4L2Format::RGB:
			v4l2Format.fmt.pix.pixelformat = V4L2_PIX_FMT_RGB24;
			v4l2Format.fmt.pix.colorspace = V4L2_COLORSPACE_DEFAULT;
			v4l2Format.fmt.pix.xfer_func = V4L2_XFER_FUNC_DEFAULT;
			break;
	}

    // Backup format for comparison
    v4l2_format requestedFormat(v4l2Format);

    // Set requested format
    if(ioctl(fd, VIDIOC_S_FMT, &v4l2Format) == -1)
    {
	    BOOST_LOG_TRIVIAL(error) << "Failed to set video device format: " << errno;
        return false;
    }

    // Read again currently set format
    if(ioctl(fd, VIDIOC_G_FMT, &v4l2Format) == -1)
    {
	    BOOST_LOG_TRIVIAL(error) << "Failed to retrieve video device format: " << errno;
        return false;
    }

    if(v4l2Format.fmt.pix.height != requestedFormat.fmt.pix.height ||
	    v4l2Format.fmt.pix.width != requestedFormat.fmt.pix.width ||
	    v4l2Format.fmt.pix.pixelformat != requestedFormat.fmt.pix.pixelformat)
    {
	    BOOST_LOG_TRIVIAL(error) << "Failed to set video device format: " << errno;
        return false;
    }

    this->bufferLength = v4l2Format.fmt.pix.sizeimage;
    return true;
}

bool v4l2::WriteFrame(uint8_t *buffer, size_t length)
{
	if(format == V4L2Format::RAW)
	{
		if(length != bufferLength)
		{
			BOOST_LOG_TRIVIAL(error) << "Incorrect frame size.";
			return false;
		}
		return write(fd, buffer, length) == bufferLength;
	}

	else //if(format == V4L2Format::RGB)
	{
		uint16_t* buffer16 = reinterpret_cast<uint16_t*>(buffer);
		size_t length16 = length / sizeof(uint16_t);

		// linear normalization
		const auto [min, max] = std::minmax_element(buffer16, buffer16 + length16);
		auto range = *max - *min;

		#pragma omp simd
		for (int i = 0; i < length16; ++i)
		{
			uint8_t value = 255UL * (buffer16[i] - *min) / range;
			rgbFrameBuffer[3*i] = value;
			rgbFrameBuffer[3*i+1] = value;
			rgbFrameBuffer[3*i+2] = value;
		}

		return write(fd, rgbFrameBuffer.data(), rgbFrameBuffer.size()) == bufferLength;
	}
}