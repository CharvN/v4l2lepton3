/* ***************************************************************************
 *  Copyright(C) 2020 Michal Charvát
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  ***************************************************************************
 */

#include "log.hh"

#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

namespace logging = boost::log;
namespace keywords = boost::log::keywords;

void init_logging(const char* fileName)
{
	logging::register_simple_formatter_factory<logging::trivial::severity_level, char>("Severity");
	logging::add_file_log(
			keywords::file_name = fileName,
			keywords::format = "[%TimeStamp%] [%ProcessID%] [%Severity%] %Message%",
			keywords::open_mode = std::ios_base::app,
			keywords::rotation_size = 10 * 1024 * 1024,
			boost::log::keywords::auto_flush = true,
			keywords::max_files = 5
	);
	logging::add_console_log(std::cout, keywords::format = "[%TimeStamp%] [%ProcessID%] [%Severity%] %Message%");

	//	logging::core::get()->set_filter(logging::trivial::severity >= logging::trivial::info);
	logging::add_common_attributes();
}