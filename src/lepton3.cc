/* ***************************************************************************
 *  Copyright(C) 2020 Michal Charvát
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  ***************************************************************************
 */

#include "log.hh"
#include "fps.hh"
#include "lepton3.hh"

#include <future>
#include <cstring>

#define SPI_SPEED 20000000ULL

// Pointer arithmetics
#define PACKET_OF_SEGMENT(segment, i)                   ((segment) + PacketSize * (i))
#define PACKETDATA_OF_SEGMENT(segment, i)               ((segment) + (PacketSize - PacketDataSize) + PacketSize * (i))
#define PACKETDATA_OF_FRAGMENT(fragment, segment, i)    ((fragment) + (SegmentSize * PacketDataSize) * (segment) + PacketDataSize * (i))

// Lower 4 bits of first byte of packet indicating unusable packet by 0xF
#define IS_PACKET_INVALID(segmentBuffer, packetIndex)   ((PACKET_OF_SEGMENT(segmentBuffer, packetIndex)[0] & 0xF) == 0xF)
// Valid segments contains segment number (starting at 1) in high 4 bits of first byte of packet[20]
#define GET_SEGMENT_NUMBER(segmentBuffer)               ((PACKET_OF_SEGMENT(segmentBuffer, 20)[0]) >> 4)


using namespace std;

inline uint16_t SwapBytesBELE(uint16_t data)
{
    return static_cast<uint16_t>(((data << 8) & 0xFF00) | ((data >> 8) & 0x00FF));
}


Lepton3::Lepton3(const string &spiDeviceName) : spiDeviceName{spiDeviceName}
{
	segmentBufferCurrent    = static_cast<uint8_t*>(aligned_alloc(64, sizeof(uint8_t) * SegmentSize * PacketSize));
    segmentBufferOld        = static_cast<uint8_t*>(aligned_alloc(64, sizeof(uint8_t) * SegmentSize * PacketSize));
    frameBufferCurrent      = static_cast<uint8_t*>(aligned_alloc(64, sizeof(uint8_t) * FrameBufferSize));
	frameBufferOld          = static_cast<uint8_t*>(aligned_alloc(64, sizeof(uint8_t) * FrameBufferSize));
}

Lepton3::~Lepton3()
{
	StopCapture();

	delete segmentBufferCurrent;
    delete frameBufferCurrent;
	delete segmentBufferOld;
	delete frameBufferOld;

	segmentBufferCurrent = nullptr;
	frameBufferCurrent = nullptr;
	segmentBufferOld = nullptr;
	frameBufferOld = nullptr;
}

void Lepton3::StartCapture()
{
	if(captureOn)
		return;

	BOOST_LOG_TRIVIAL(trace) << "Starting SPI capture thread.";

	captureOn = true;
	captureThread = thread(&Lepton3::Capture, ref(*this));
}

void Lepton3::StopCapture()
{
	captureOn = false;
	BOOST_LOG_TRIVIAL(trace) << "Stopping SPI capture thread.";

	if(captureThread.joinable())
		captureThread.join();
}

void Lepton3::Capture()
{
	while(captureOn)
	{
		restartCapture:

		try
		{
			SPI spi;
			if (!spi.Open(spiDeviceName, SPI_SPEED))
			{
				BOOST_LOG_TRIVIAL(error) << "Failed opening SPI device: " << spiDeviceName;
				this_thread::sleep_for(chrono::seconds(2));
				continue;
			}

			FpsCounter fps(10000);

			// Capture initial segment
			future<bool> segmentReady = async(launch::async, &Lepton3::ReadSegment, this, cref(spi));

			while (captureOn)
			{
				// FrameSize segments in a frame
				for (uint32_t segmentIndex = 0; segmentIndex < FrameSize;)
				{

					// Wait for available segment
					if (!segmentReady.get())
					{
						BOOST_LOG_TRIVIAL(error) << "Failed retrieving frame segment." << endl;
						goto restartCapture;
					}

					// After stop signaled, dont process last segment
					if(!captureOn)
						break;

					// Swap buffers and start a new segment capture straight away
					swap(segmentBufferCurrent, segmentBufferOld);
					segmentReady = async(launch::async, &Lepton3::ReadSegment, this, cref(spi));


					uint8_t segmentNumber = GET_SEGMENT_NUMBER(segmentBufferOld);
					if(segmentNumber == 0)
						continue;

					if (segmentNumber != segmentIndex + 1)
					{
						BOOST_LOG_TRIVIAL(debug) << "Received segment number " << static_cast<int>(segmentNumber) << ", while expecting " << segmentIndex + 1;
						continue;
					}

					// Copy segment image data into frame buffer (stripping header) and change endianness
					for (uint32_t segmentDataIndex = 0; segmentDataIndex < SegmentSize; segmentDataIndex++)
					{
						// Strip header
						uint8_t *packetDataFragmentDestination = PACKETDATA_OF_FRAGMENT(frameBufferCurrent, segmentIndex, segmentDataIndex);
						memcpy(packetDataFragmentDestination, PACKETDATA_OF_SEGMENT(segmentBufferOld, segmentDataIndex), PacketDataSize);
					}

					segmentIndex++;
				}

				// Frame ready, swap buffers and indicate it is ready
				{
					fps.Tick();
					unique_lock<mutex> frameLock(frameBufferMutex);
					if (frameIsRequested)
					{
						swap(frameBufferCurrent, frameBufferOld);
						frameLock.unlock();
						frameReady.notify_one();
					}
					else if(captureOn)
						BOOST_LOG_TRIVIAL(trace) << "Dropped a frame - got a new one before previous was sent out.";
				}
			}
		}
		catch(exception &e)
		{
			BOOST_LOG_TRIVIAL(error) << "Exception in frame capture process: " << e.what();
		}
	}
}


bool Lepton3::ReadSegment(const SPI& spi)
{
	restartReadSegment:

	uint32_t packetIndex = 0;
    while(captureOn)
	{
        // Read the first packet to the current segment buffer
        if(!spi.Transfer(nullptr, PACKET_OF_SEGMENT(segmentBufferCurrent, packetIndex), PacketSize))
            return false;

        if(IS_PACKET_INVALID(segmentBufferCurrent, packetIndex))
        {
            // Wait for a valid packet
	        this_thread::sleep_for(chrono::microseconds(1));
	        continue;
        }

        // We got the first valid packet, lets grab the rest of the segment
        packetIndex++;

        if(!spi.Transfer(nullptr, PACKET_OF_SEGMENT(segmentBufferCurrent, packetIndex), PacketSize * (SegmentSize - 1)))
			return false;

        // Check packet ordering
		for(; packetIndex < SegmentSize; packetIndex++)
		{
			uint8_t receivedPacketNumber = PACKET_OF_SEGMENT(segmentBufferCurrent, packetIndex)[1];
			if (IS_PACKET_INVALID(segmentBufferCurrent, packetIndex) || receivedPacketNumber != packetIndex)
			{
				BOOST_LOG_TRIVIAL(warning) << "Synchronization lost at packet: " << packetIndex;

				// Resynchronize camera
				this_thread::sleep_for(chrono::milliseconds(200));
				goto restartReadSegment;
			}
		}
		return true;
    }
    return true;
}

const uint8_t *Lepton3::GetFrame(int64_t timeoutMs)
{
	unique_lock<mutex> frameLock(frameBufferMutex);

	// Indicate someone is waiting for a frame, wait for a frame to be ready
	frameIsRequested = true;
	auto waitResult = frameReady.wait_for(frameLock, chrono::milliseconds(timeoutMs));
	frameIsRequested = false;

	if(waitResult == cv_status::timeout)
		return nullptr;

	// The data in the buffer wont be changed until frameIsRequested is set to true
	return frameBufferOld;
}
