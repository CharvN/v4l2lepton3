/* ***************************************************************************
 *  Copyright(C) 2020 Michal Charvát
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  ***************************************************************************
 */

#include "fps.hh"
#include "log.hh"

void FpsCounter::Tick()
{
	frameCounter++;
	auto now = std::chrono::steady_clock::now();

	auto differenceMs = std::chrono::duration_cast<std::chrono::milliseconds>(now - startTime).count();
	if(differenceMs >= reportIntervalMs)
	{
		float fps = frameCounter * 1000.f / differenceMs;
		BOOST_LOG_TRIVIAL(trace) << "FPS: " << fps;

		startTime = now;
		frameCounter = 0;
	}
}