/* ***************************************************************************
 *  Copyright(C) 2020 Michal Charvát
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  ***************************************************************************
 */

#pragma once

#include <string>
#include <linux/spi/spidev.h>

class SPI
{
protected:
	int fd = {-1};
	uint8_t mode;
	spi_ioc_transfer transferParam;

public:
	SPI() : fd{-1}, mode{0xFF}, transferParam{0} {}

	SPI(SPI &&other) = default;
	SPI(const SPI &other) = delete;
	~SPI() { Close(); }

	operator bool() const {return fd >= 0;}
	SPI& operator=(SPI &&other) = default;
	SPI& operator=(const SPI &other) = delete;

	bool Open(const std::string &deviceName, uint32_t speed, uint16_t delayUs = 0, uint8_t csChange = 0, uint8_t mode = 3, uint8_t bitsPerWord = 8);
	bool Transfer(const uint8_t *transmitBuffer, const uint8_t *receiveBuffer, uint32_t length) const;
	void Close();

	bool setMode(uint8_t mode);
	bool setSpeed(uint32_t speedHz);
	bool setBitsPerWord(uint8_t bitsPerWord);
};