/* ***************************************************************************
 *  Copyright(C) 2020 Michal Charvát
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *  ***************************************************************************
 */

#include "fps.hh"
#include "log.hh"
#include "v4l2.hh"
#include "lepton3.hh"
#include "opt_parser.hh"

#include <csignal>
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include <boost/endian/conversion.hpp>
#include <boost/iostreams/filter/zlib.hpp>
#include <boost/iostreams/filtering_stream.hpp>

namespace io = boost::iostreams;
namespace asio = boost::asio;
using boost::asio::ip::tcp;
using namespace std;

atomic<bool> shouldTerminate{false};

void SignalHandler(int signum){ BOOST_LOG_TRIVIAL(info) << "Received SIGINT. Exiting."; shouldTerminate = true; }

const static char Help[] = "Usage:\nclient -i <ipv4> [-p <port>] [-v <video_dev>] [-c]\n"
                           "   -h --help -- shows help \n"
						   "   -i --ip <ipv4> -- ip address of the server\n"
                           "   -p --port <port> -- port of the server, default `2222`\n"
                           "   -v --video <video_dev> -- name of a video loopback device, default `/dev/video0`\n"
                           "   -c --compressed -- turns on frame decompression\n"
						   "   -r --raw -- uses the v4l2 video device in Y16 format, default RGB24 (lin. normalized)";

int main(int argc, const char* argv[])
{
	init_logging("client.%N.log");
	signal(SIGINT, SignalHandler);


	// Parse arguments
	OptParser arguments;
	arguments.RegisterOption("-h", "--help");
	arguments.RegisterOption("-i", "--ip", false, Option::RequiredParam);
	arguments.RegisterOption("-p", "--port", true, Option::RequiredParam, "2222");
	arguments.RegisterOption("-v", "--video", true, Option::RequiredParam, "/dev/video5");
	arguments.RegisterOption("-c", "--compressed", true);
	arguments.RegisterOption("-r", "--raw", true);
	arguments.ParseArguments(argc, argv);

	if (arguments.GetOption("-h")->entered)
	{
		BOOST_LOG_TRIVIAL(info) << Help;
		return 0;
	}
	if (!arguments.Valid())
	{
		BOOST_LOG_TRIVIAL(fatal) << "Invalid arguments.";
		BOOST_LOG_TRIVIAL(info) << Help;
		return 1;
	}


	try
	{
		boost::array<uint8_t, Lepton3::FrameBufferSize> frame = {0};

		BOOST_LOG_TRIVIAL(trace) << "Opening video device: " << arguments.GetOption("-v")->param.c_str();

		v4l2::V4L2Format format = arguments.GetOption("-r")->entered ? v4l2::V4L2Format::RAW : v4l2::V4L2Format::RGB;
		v4l2 videoDevice(Lepton3::FrameWidth, Lepton3::FrameHeight, format);

		if(!videoDevice.Open(arguments.GetOption("-v")->param.c_str()))
			throw runtime_error("Failed opening video device.");

		boost::asio::io_service io_service;
		tcp::resolver resolver(io_service);

		const string &ip = arguments.GetOption("-i")->param;
		const string &port = arguments.GetOption("-p")->param;
		bool compressionOn = arguments.GetOption("-c")->entered;
		BOOST_LOG_TRIVIAL(trace) << "Connecting to " << ip << ":" << port;

		tcp::resolver::query query(ip, port);
		tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);

		tcp::iostream socket;
		socket.connect(ip, port);
		if(!socket)
			throw runtime_error("Failed connecting to the server.");

		io::filtering_istream decompresingStream;
		if(compressionOn) decompresingStream.push(io::zlib_decompressor());
		decompresingStream.push(socket);

		FpsCounter fps(10000);
		BOOST_LOG_TRIVIAL(trace) << "Connected.";

		try
		{
			while (!shouldTerminate)
			{
				// Get frame
				decompresingStream.read(reinterpret_cast<char *>(frame.data()), frame.size());

				// Reverse order of bytes
				{
					size_t frameSize = frame.size() / sizeof(uint16_t);
					uint16_t* framePointer = reinterpret_cast<uint16_t*>(frame.data());

					#pragma omp simd
					for(size_t i = 0; i < frameSize; ++i)
						framePointer[i] = boost::endian::endian_reverse(framePointer[i]);
				}


				// Process frame
				videoDevice.WriteFrame(frame.data(), frame.size());
				fps.Tick();
			}
		}
		catch(exception &e)
		{
			BOOST_LOG_TRIVIAL(warning) << "Lost connection to the server: " << e.what();
			return 2;
		}

		return 0;
	}

	catch (std::exception& e)
	{
		BOOST_LOG_TRIVIAL(fatal) << "Exception: " << e.what();
		return 3;
	}
}
