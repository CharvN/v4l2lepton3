#!/usr/bin/env python3

#  ###########################################################################
 #  Copyright(C) 2020 Michal Charvat
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU Lesser General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Lesser General Public License for more details.
 #  
 #  You should have received a copy of the GNU Lesser General Public License
 #  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #  ###########################################################################
#

import os
import cv2
import sys
import time
import logging
import argparse
import numpy as np
from datetime import datetime
from logging.handlers import RotatingFileHandler

from v4l2lepton3.utils import Normalize_lin8, Normalize_clahe

def write_raw_image(frame: np.ndarray, directory:str, filename: str):
	full_file_name = os.path.join(directory, filename + '.tiff')
	cv2.imwrite(full_file_name, frame)
	log.info('Written raw image: {}'.format(full_file_name))

def write_normalized_image(frame: np.ndarray, directory: str, filename: str):
	full_file_name = os.path.join(directory, filename + '.png')
	normalized_image = Normalize_lin8(Normalize_clahe(frame))
	cv2.imwrite(full_file_name, normalized_image)
	log.info('Written normalized image: {}'.format(full_file_name))

def _images_exist(directory: str, file_name: str) -> bool:
	return any(os.path.exists(os.path.join(directory, file_name + extension)) for extension in ['.tiff', '.png'])

def get_file_name(directory: str) -> str:
	timestamp = datetime.now().strftime("%Y%m%d_%H%M_%S")
	file_name = timestamp
	file_index = 0

	while _images_exist(directory, file_name):
		file_name = '{}_{}'.format(timestamp, file_index)
		file_index += 1

	return file_name


if __name__ == '__main__':
	# parse arguments
	parser = argparse.ArgumentParser(description='Script capturing a single frame from a Lepton 3.5 camera locally or remotely.')
	parser.add_argument('-s', '--spi', dest='spi', help='SPI device with the local Lepton 3.5 camera, e.g. /dev/spidev0.0')
	parser.add_argument('-i', '--ip', dest='ip', help='IP of the remote v4l2lepton3 server running for remote capture.')
	parser.add_argument('-p', '--port', dest='port', default=2222, type=int, help='PORT of the remote v4l2lepton3 server running for remote capture. Default 2222')
	parser.add_argument('-d', '--directory', dest='directory', default='.', help='Directory into which the frames should be saved. Default .')
	parser.add_argument('-r', '--repeat', dest='delay', type=float, help='Repeated capturing after <delay> seconds until SIGINT.')
	parser.add_argument('-t', '--timeout', type=int, default=45, help='Timout to get a frame. If it is reached, the script exits with 2. Default 45')
	args = parser.parse_args()

	assert os.path.isdir(args.directory), 'Save directory must exist: {}'.format(args.directory)
	assert (args.spi is None) != (args.ip is None), 'The script must be run either with a remote IP of a v4l2lepton3 server or with a local SPI device exclusively.'

	# init logging
	log = logging.getLogger(__name__)
	log.setLevel(logging.DEBUG)
	formatter = logging.Formatter('[{levelname}] {asctime} | {funcName}(): {message}', datefmt='%y/%m/%d %H:%M:%S', style='{')

	file_handler = RotatingFileHandler(os.path.join(args.directory, 'lepton3capture.log'), maxBytes=10 * 1024 * 1024, backupCount=5)
	file_handler.setFormatter(formatter)
	file_handler.setLevel(logging.DEBUG)
	log.addHandler(file_handler)

	console_handler = logging.StreamHandler(sys.stdout)
	console_handler.setFormatter(formatter)
	console_handler.setLevel(logging.DEBUG)
	log.addHandler(console_handler)

	if args.spi:
		from v4l2lepton3.capture import Lepton3Capturer
		capturer = Lepton3Capturer(args.spi)
	else:
		from v4l2lepton3.client import Lepton3Client
		capturer = Lepton3Client(args.ip, args.port)

	while(True):
		try:
			with capturer:
				frame = capturer.get_frame(timeout=args.timeout)

			log.info('OK. Frame received.')
			file_name = get_file_name(args.directory)
			write_raw_image(frame, args.directory, file_name)
			write_normalized_image(frame, args.directory, file_name)

			if args.delay is None:
				break

			log.info('Sleeping for: {} s'.format(args.delay))
			time.sleep(args.delay)

		except KeyboardInterrupt:
			log.info('SIGINT received. Exiting.')
			break

		except TimeoutError:
			log.error('Failed capturing a frame: Timeout has been reached: {} s'.format(args.timeout))
			exit(2)

		except Exception as e:
			log.error('Failed capturing a frame: {}'.format(e))
			exit(3)
