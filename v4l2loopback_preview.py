#!/usr/bin/env python3

#  ###########################################################################
 #  Copyright(C) 2020 Michal Charvat
 #
 #  Script that sequentially captures a single frame from a Lepton3 camera
 #  after a specified delay and saves captured image in a file or in a text
 #  form.
 #
 #  This program is free software: you can redistribute it and/or modify
 #  it under the terms of the GNU Lesser General Public License as published by
 #  the Free Software Foundation, either version 3 of the License, or
 #  (at your option) any later version.
 #
 #  This program is distributed in the hope that it will be useful,
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #  GNU Lesser General Public License for more details.
 #
 #  You should have received a copy of the GNU Lesser General Public License
 #  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #  ###########################################################################
#

import sys
import cv2

if __name__ == '__main__':
    assert len(sys.argv) >= 2, 'Usage ./v4l2loopback_preview.py <video_device_number>'
    video_device = int(sys.argv[1])
    window_name = 'Video device: {}'.format(video_device)

    print('Opening video device: {}'.format(video_device))
    cap = cv2.VideoCapture(video_device)
    assert cap.isOpened(), 'Failed opening video device: {}'.format(video_device)

    try:
        while (True):
            ret, frame = cap.read()
            cv2.imshow(window_name, frame)

            key = cv2.waitKey(1) & 0xFF
            if key == 27: # ESC
                break

    except KeyboardInterrupt:
        pass

    finally:
        cap.release()
        cv2.destroyAllWindows()